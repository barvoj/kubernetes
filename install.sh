#!/bin/bash
set -xeuo pipefail

NODE_IP=$1
GATEWAY_IP=$2

# Replace ${PUBLIC_IP} var in file
sed "s/\${PUBLIC_IP}/${NODE_IP}/g; s/\${GATEWAY_IP}/${GATEWAY_IP}/g" cloud_config.yml  > /tmp/cloud_config.yml

# Set up node
sudo coreos-install -d /dev/vda -C stable -c /tmp/cloud_config.yml
