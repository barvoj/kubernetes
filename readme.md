# Create one node kubernetes cluster

Download coreos iso from https://coreos.com/os/docs/latest/booting-with-iso.html

Mount iso to optical drive and wait til it boots. Run:

Create /etc/systemd/network/static.network

    [Match]
    Name=eth0
    
    [Network]
    DNS=8.8.8.8
    Address=185.8.239.51/24
    Gateway=185.8.239.1
    
sudo systemctl restart systemd-networkd

    git clone https://gitlab.com/barvoj/kubernetes.git
    cd kubernetes
    chmod +x install.sh
    ./install.sh 185.8.239.51 185.8.239.1

After complete remove iso from optical drive and restart.

Sign in as app/app.

Now download bootkube from https://github.com/kubernetes-incubator/bootkube and run

    ./bootkube render  --api-server-alt-names=IP=185.8.239.51,DNS=bart --api-servers=185.8.239.51 --asset-dir=/home/app/bart --experimental-self-hosted-etcd
    scp -r ./bart/ app@185.8.239.51:/home/app
    scp /c/Users/Barvoj/Downloads/bootkube-0.6.2/bin/linux/bootkube app@185.8.239.51:/home/app
    ssh app@185.8.239.51 'sudo cp /home/app/bart/auth/kubeconfig /etc/kubernetes/'
    ssh app@185.8.239.51 'sudo cp /home/app/bart/tls/ca.crt /etc/kubernetes/'
    ssh app@185.8.239.51 'sudo mkdir -p /etc/etcd/tls/etcd'
    ssh app@185.8.239.51 'sudo cp /home/app/bart/tls/etcd/* /etc/etcd/tls/etcd/'
    ssh app@185.8.239.51 'sudo cp /home/app/bart/tls/etcd-* /etc/etcd/tls/'
    ssh app@185.8.239.51 'sudo ./bootkube start --asset-dir=/home/app/bart/'
    
Mesuring
    
    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/grafana.yaml
    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
    
    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
    
Dashboard

    kubectl --kubeconfig=kubeconfig apply -f https://git.io/kube-dashboard
    
Ingress controller

    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml
    kubectl --kubeconfig=kubeconfig apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-deployment.yaml
    
    kubectl --kubeconfig=kubeconfig apply -f ./traefik.yml
